FROM ubuntu:latest
MAINTAINER Rod Mooseman
RUN apt-get update
RUN apt-get install -y figlet
RUN figlet Rod
COPY h.sh /
RUN chmod 744 h.sh
RUN ./h.sh mayra

